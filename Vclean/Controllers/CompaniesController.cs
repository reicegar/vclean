﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Vclean.Common.Models;
using Vclean.Models;
using Vclean.Helpers;

namespace Vclean.Controllers
{
    public class CompaniesController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: Companies
        public async Task<ActionResult> Index()
        {
            return View(await db.Companies.ToListAsync());
        }

        // GET: Companies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var company = await db.Companies.FindAsync(id);

            if (company == null)
            {
                return HttpNotFound();
            }

            var view = new DetailCompanyView
            {
                Name = company.Name,
                CompanyId = company.CompanyId,
                CompanyCode = company.CompanyCode,
                ImagePath = company.ImagePath,
                Sites = company.Sites
            };

            return View(view);
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CompanyView view)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                var folder = "~/Content/Images/CompanyLogo";

                if(view.ImageFile != null)
                {
                    pic = FileHelper.UploadFile(view.ImageFile, folder);
                    pic = $"{folder}/{pic}";
                }

                var company = new Company
                {
                    CompanyCode = view.CompanyCode,
                    ImagePath = pic,
                    Name = view.Name
                };

                db.Companies.Add(company);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(view);
        }

        // GET: Companies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }

            var view = new CompanyView
            {
                Name = company.Name,
                CompanyCode = company.CompanyCode,
                CompanyId = company.CompanyId,
                ImagePath = company.ImagePath                
            };

            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(CompanyView view)
        {
            if (ModelState.IsValid)
            {
                var pic = view.ImagePath;
                var folder = "~/Content/Images/CompanyLogo";

                if (view.ImageFile != null)
                {
                    pic = FileHelper.UploadFile(view.ImageFile, folder);
                    pic = $"{folder}/{pic}";
                }

                var company = new Company
                {
                    Name = view.Name,
                    CompanyCode = view.CompanyCode,
                    CompanyId = view.CompanyId,
                    ImagePath = pic
                };

                db.Entry(company).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(view);
        }

        // GET: Companies/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Company company = await db.Companies.FindAsync(id);
            db.Companies.Remove(company);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddSite(DetailCompanyView view)
        {
            //Don't need validate the model because only is using one string
            var site = new Site
            {
                SiteName = view.SiteName,
                Address = view.Address,
                OptionalAddress = view.OptionalAddress,
                City = view.City,
                PostCode = view.PostCode,
                CompanyId = view.CompanyId                
            };

            db.Sites.Add(site);
            await db.SaveChangesAsync();

            return RedirectToAction($"Details/{view.CompanyId}");

        }

        public async Task<ActionResult> EditSite(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var site = await db.Sites.FindAsync(id);
            if (site == null)
            {
                return HttpNotFound();
            }

            var view = new Site
            {
                CompanyId = site.CompanyId, 
                SiteName = site.SiteName,
                SiteId = site.SiteId,
                Address = site.Address,
                OptionalAddress = site.OptionalAddress,
                City = site.City,
                PostCode = site.PostCode
            };

            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditSite(Site site)
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(site).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction($"Details/{site.CompanyId}");
            }
            return View(site);
        }

        public async Task<ActionResult> DeleteSite(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var site = await db.Sites.FindAsync(id);
            if (site == null)
            {
                return HttpNotFound();
            }

            db.Sites.Remove(site);
            await db.SaveChangesAsync();

            return RedirectToAction($"Details/{site.CompanyId}");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

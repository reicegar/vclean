﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Vclean.Startup))]
namespace Vclean
{
    using System.Linq;
    using System.Web.Configuration;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CheckUserAdmin();
        }

        private void CheckUserAdmin()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole { Name = "Admin" };
                roleManager.Create(role);
                
            }

            if (!roleManager.RoleExists("User"))
            {
                var role = new IdentityRole { Name = "User" };
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Driver"))
            {
                var role = new IdentityRole { Name = "Driver" };
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Customer"))
            {
                var role = new IdentityRole { Name = "Customer" };
                roleManager.Create(role);
            }


            var roleId = context.Roles.Where(r => r.Name == "Admin").Select(r => r.Id).FirstOrDefault();

            var result = context.Users.Where(u => u.Roles.Any(r => r.RoleId == roleId)).FirstOrDefault();

            if (result == null)
            {
                var user = new ApplicationUser
                {
                    UserName = WebConfigurationManager.AppSettings["AdminUser"],

                };
                user.Email = user.UserName;
                user.EmailConfirmed = true;

                string userPWD = WebConfigurationManager.AppSettings["AdminPassWord"];

                var chkUser = UserManager.Create(user, userPWD);

                //Add default User to Role Admin
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Admin");
                }
            }
            

        }

        }

    }

    


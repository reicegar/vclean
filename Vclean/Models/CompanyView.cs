﻿namespace Vclean.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web;
    using Common.Models;

    [NotMapped]
    public class CompanyView : Company
    {
        public HttpPostedFileBase ImageFile { get; set; }
    }
}
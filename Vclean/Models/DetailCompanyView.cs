﻿namespace Vclean.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Vclean.Common.Models;

    [NotMapped]
    public class DetailCompanyView : Company
    {
        [StringLength(50)]
        public string SiteName { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string OptionalAddress { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [Required]
        public string PostCode { get; set; }
    }
}
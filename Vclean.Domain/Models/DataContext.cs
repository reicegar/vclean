﻿namespace Vclean.Domain.Models
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public abstract class DataContext : DbContext
    {
        public DataContext() : base ("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<Common.Models.Company> Companies { get; set; }

        public DbSet<Common.Models.Site> Sites { get; set; }
    }
}

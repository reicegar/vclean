﻿namespace Vclean.Common.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Company
    {
        [Key]
        public int CompanyId { get; set; }

        [Required]
        [Display(Name = "Company")]
        [MaxLength(50, ErrorMessage = "Sorry, maximum characters are {0}")]
        public string Name { get; set; }

        [Display(Name = "Image")]
        public string ImagePath { get; set; }

        [Required]
        [Display(Name = "Initials")]
        [StringLength(3, ErrorMessage = "The maximum characters are 3")]        
        public string CompanyCode { get; set; }

        public virtual ICollection<Site> Sites { get; set; }
    }
}

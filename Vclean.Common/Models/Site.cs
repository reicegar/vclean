﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vclean.Common.Models
{
    public class Site
    {
        [Key]
        public int SiteId { get; set; }

        [StringLength(50)]
        [Display(Name = "Building")]
        public string SiteName { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        [Display(Name = "Address 2 (optional)")]
        public string OptionalAddress { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [Required]
        public string PostCode { get; set; }

        public virtual Company Company { get; set; }

        public int CompanyId { get; set; }

    }
}
